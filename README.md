The Role of the Tech Lead
=========================

![Photo of a laptop, phone, and notebook with a pen](https://miro.medium.com/max/1280/1*ImeMahtuzmC2WsAp8QAXqQ.jpeg)

Image by [Pexels](https://pixabay.com/users/pexels-2286921/)

The technical lead (or tech lead) is a common role at software companies. One of the challenges in defining the tech lead job description is that the shape of the role varies widely from company to company --- sometimes even from team to team.

That said, everyone from product managers to designers to engineering managers benefit when there is a shared understanding of what the tech lead's responsibilities are. So, in this article, we break down

-   what the tech lead role is
-   what they're responsible for
-   how the tech lead differs from the engineering manager
-   how team members can collaborate best with the tech lead

At the highest level, a tech lead role encompasses a set of responsibilities that a senior software engineer takes on to lead the technical execution of products or features for their team. They also act as a liaison with other software teams, provide coding mentorship to more junior software engineers, and collaborate with an engineering manager to identify and prioritize technical debt.

Curious about the role of the tech lead? Want to learn if perhaps that's the next step in your career or find out more about what it's like to work with a tech lead? Book some time with [a Merit mentor](https://www.get-merit.com/discover) to get advice.

The Tech Lead Job Description
=============================

The tech lead role presents an opportunity for software engineers to learn new skills and expand their influence. A tech lead is willing to "play the role of architect, project manager, software engineer, mentor, and teammate all at once," [according to Tyler Hawkins](https://betterprogramming.pub/8-lessons-i-learned-as-a-tech-lead-3c41373070b0). For many senior software engineers, the tech lead role is a step to becoming a leader within the organization.

In most companies, the tech lead is a role that a senior software engineer adopts, but it isn't a promotion or title change. It's a bit misleading to provide a "job description" for a tech lead, because software engineers don't interview for a tech lead job --- they become one once they've joined the company. If you're at an org and decide to become a tech lead, more often than not, you don't get a raise. As Becca Liss, senior software engineer at VTS and former tech lead, puts it, "I think one of the key things to remember is a tech lead [has] no power. It comes with no payroll increase."

A software engineer might transition into being a tech lead and later decide to take off the tech lead hat. This shift isn't a demotion or a loss of seniority; instead, the engineer is choosing to focus on a different set of skills and responsibilities. If they'd like to focus instead more on coding, they may choose to stop being a tech lead, at least temporarily.

Key responsibilities of a tech lead
===================================

At a high level, the tech lead "gets the ship going in the right direction from a technical perspective" and has "key ownership of the execution phase," according to [Niko Lazaris](https://www.get-merit.com/p/niko-lazaris), senior software engineer and tech lead at Drift.

What are those responsibilities? In general, the tech lead focuses on the "how":

-   how to build a feature
-   how to architect a system
-   how to implement new patterns and paradigms
-   how to adopt new technologies
-   how to tackle [technical debt](https://www.productplan.com/glossary/technical-debt/)

The tech lead spends less time on the "why" and prioritization. Although strong tech leads work with a product manager to understand the needs of customers and business priorities, the tech lead isn't responsible for user interviews, roadmapping, working with stakeholders, or communicating to leadership about team priorities and challenges.

Rey Rodrigues, software engineer at Relay Payments and a former engineering manager and tech lead, says that this role is a bit of a "double-edged sword. You have the leadership that you're expected to deliver on something, but you also have little power to affect how you're going to deliver on it...you're not able to control resources. That's your engineering managers [who] are able to control timelines."

In practice, the focus on the "how" ends up looking like a tech lead performing some combination of these tasks:

-   Writing [RFCs](https://blog.pragmaticengineer.com/scaling-engineering-teams-via-writing-things-down-rfcs/) (request for comments documents) for new products or architectural endeavors
-   Reviewing technical debt with the engineering manager (the engineering manager might use these an inputs for shaping a team's [technical roadmap](https://www.aha.io/roadmapping/guide/roadmap/technology-roadmap))
-   Discussing [product requirements](https://www.productplan.com/glossary/product-requirements-document/) with product managers and designers to assess technical [feasibility](https://www.svpg.com/discovery-judgement/) and estimated scope
-   Acting as the technical point-of-contact with other teams when those teams have questions about an area of the codebase that the tech lead's team owns

![A venn diagram describing the relationship between the engineering manager, technical lead, and product manager](https://miro.medium.com/max/1400/0*XdsacZ8UjwWPch_P.png)

In this diagram, you can think of the "eng lead" as the same as the tech lead. Source: [Lara Hogan](https://larahogan.me/blog/team-leader-venn-diagram/)

Tech Lead vs. Engineering Manager
=================================

Many companies separate the role of the technical lead and the job of the engineering manager. Some companies combine the two, although this is less common. The titles sound pretty similar though --- one is a manager, one is a lead, and they both have to do with technology. So what's the difference?

At a high level, the engineering manager

-   focuses on people management, like coaching and helping software engineers set and reach career goals
-   acts as the product manager's counterpart in communicating with leadership on the team's roadmap and progress
-   calls out technical and personnel risks for new products and features
-   ensures that the team has the time, people, and resources to do its best work

Additionally, the engineering manager is concerned with the team's morale and its ability to deliver outcomes and meet metrics, like [velocity](https://www.atlassian.com/agile/project-management/metrics#:~:text=Velocity,-Velocity) and [code quality](https://stackoverflow.blog/2021/10/18/code-quality-a-concern-for-businesses-bottom-lines-and-empathetic-programmers/).

Liss describes her experience of engineering management: "The two big responsibilities for the engineering manager are the growth of the individuals and being the bridge between [the team] and leadership."

In contrast, the tech lead defines the outline for building a new feature and helps write the code for these features, while the engineering manager spends much less (if any) time in the codebase.

However, the two roles do have some overlap. The two will most likely collaborate closely on the [technical roadmap](https://miro.com/templates/technology-roadmap/). The tech lead, because they spend so much time in the codebase, will be able to explain what debt needs to be addressed and how, and the engineering manager will translate that into a priority to be communicated to leadership.

If the team is struggling to meet a deadline or if a project is getting off track, the tech lead will be able to explain to the engineering manager any technical headwinds or challenges that are affecting the team's progress.

The engineering manager also plays an important role in determining if a feature can be built by the team by a certain point in time. The tech lead will help the product manager understand the tactical nuts-and-bolts of building something, while the engineering manager will be evaluating if the estimation and scope aligns with leadership's expectations.

Putting on the Tech Lead Hat
============================

When software engineers become tech leads, they'll experience some changes in their daily work activities, even though they haven't changed titles or been promoted. Most likely, a tech lead will be

-   attending more meetings, because their focus is expanding from writing code to collaborating with product and design
-   liaising more with external teams when work overlaps in the codebase
-   participating in more pair programming
-   providing code mentorship to junior software engineers

As a result, the overall time spent coding might go down, while the time spent in meetings and writing documentation may go up. Rodrigues says, "It's a different skill set that you have to learn...it's very interpersonal. You have to be able to explain yourself with more words than you would with someone you consider a direct peer...you are explaining concepts that are sometimes foreign to people, and if you're not able to [explain] in a way that they are able to internalize, you'll have a hard time building empathy." But it's not just explaining: "Listening is an important part of the job, too."

For many tech leads who transition while staying at the same company, starting the role includes little onboarding. Because tech leads aren't people managers, they don't receive management training, and because they're not joining the organization for the first time as a new hire, they don't receive hands-on "here's how to perform your role well."

Working with Tech Leads
=======================

In general, most product teams have a ["trio" breakdown of product, design, and engineering](https://www.svpg.com/four-big-risks/).

-   The product manager is concerned with *value* (customers will want to buy or use the product) and *viability* (the products align with other aspects of the business)
-   The designer is concerned with *usability* (if users can figure out how to use the product)
-   The tech lead and engineering manager are focused on *feasibility* (can engineers build the product to spec within the timeline)

Engineering managers in particular are focused on if the existing team is large enough and has the right skill sets to build the product within the determined timeframe.

![A venn diagram showing how engineering, design, and product management contribute to a feature](https://miro.medium.com/max/1400/0*N2JA5kIz-jxp2sn8.jpg)

In practice, this means that software engineers, product managers and designers, and engineering managers collaborate with tech leads depending on their role:

-   Product managers and designers most often ask about changes to scope if the requirements are modified or the technical trade-offs of implementing a feature.
-   Engineering managers will most likely ask the tech lead's opinion on scope, technical risks, and prioritizing technical debt.
-   Software engineers might ask about help with debugging and questions about the why or how of implementing a new architecture or paradigm.
